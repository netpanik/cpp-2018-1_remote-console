#pragma once
#include <iostream>
#include <WS2tcpip.h>
#include <string>
#include "Network.h"
#pragma comment(lib,"ws2_32.lib")

Server::Server(int protocol, int sock, int flags)
{
	int wsOk = WSAStartup(ver, &wsData);

	if (wsOk != 0)
		throw std::exception("Socket startup failed");
	
	listening = socket(protocol, sock, flags);
	if (listening == INVALID_SOCKET)
		throw std::exception("Socket creation failed");
}

void Server::key_exchange()
{
	generateKey(n, e, d);
	const int size = sizeof(unsigned long int) * 2;
	char key[size];
	memcpy(&key[0], &n, sizeof(unsigned long int));
	memcpy(&key[1], &e, sizeof(unsigned long int));
	c_send(key, size);
}
void Server::cr_serialize(char* str, char * secureStr, char  cNumRead)
{
	cryptSerialize(&str[1], &secureStr[1], cNumRead * 20 + 1, e, n);
}
void Server::cr_deserialize(char *secureBuf, char *buf)
{
	cryptDeserialize(secureBuf, buf, buf[0], d, n);
}
void Server::binding(int family, int port, ULONG addr)
{
	hint.sin_family = family;
	hint.sin_port = htons(port);
	hint.sin_addr.S_un.S_addr = addr;
	std::cout << "Listening on port: " << port << std::endl;
	bind(listening, (sockaddr*)&hint, sizeof(hint));
}
bool Server::establish(char* msg)
{
	const int size = sizeof(msg)+2;
	char r_msg[size];
	recv(clientSocket, r_msg, size, 0);
	if (strcmp(r_msg, msg))
		throw std::exception("Cannot establish connection");

	Sleep(50);
	send(clientSocket, msg, size, 0);
	return true;
}
int Server::c_send(char * buf, int buf_length) {
	int result = send(clientSocket, buf, buf_length, 0);
	if (result == SOCKET_ERROR)
		throw std::exception("Socket error");
	return result;
}
int Server::receive(char * buf, int buf_length) {
	int bytesReceived = recv(clientSocket, buf, buf_length, 0);
	if (bytesReceived == SOCKET_ERROR)
		throw std::exception("Socket error");
	return bytesReceived;
}
void Server::connection()
{
	if (listen(listening, SOMAXCONN) == SOCKET_ERROR)
	{
		wprintf(L"listen function failed with error: %d\n", WSAGetLastError());
		throw std::exception("Server::connection : SOCKET_ERROR");
	}
		
	clientSocket = accept(listening, (sockaddr*)&client, NULL);
	if (clientSocket == INVALID_SOCKET)
	{
		DWORD wsaErrro  = WSAGetLastError();
		throw std::exception("Server::connection : INVALID_SOCKET");
	}

	closesocket(listening);
}
Server::~Server()
{
	closesocket(clientSocket);
	WSACleanup();
}

// ============================================================CLIENT==============================================================

Client::Client(int protocol, int sock, int flags)
{
	int wsOk = WSAStartup(ver, &wsData);
	if (wsOk != 0)
	{
		std::cerr << "Can't initialize a socket..." << std::endl;
	}
	sock_cl = socket(protocol, sock, flags);
	if (sock_cl == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket " << std::endl;
	}
}

void Client::key_exchange()
{
	generateKey(n, e, d);
	const int size = sizeof(unsigned long int) * 2;
	char key[size];
	receive(key, size);
	memcpy(&n, &key[0], sizeof(unsigned long int));
	memcpy(&e, &key[1], sizeof(unsigned long int));
}
void Client::cr_serialize(char* str, char * secureStr,char  cNumRead)
{
	cryptSerialize(&str[1], &secureStr[1], cNumRead * 20 + 1, e, n);
}
void Client::cr_deserialize(char *secureBuf, char *buf)
{
	cryptDeserialize(secureBuf, buf, buf[0], d, n);
}
bool Client::establish(char* msg)
{
	const int size = sizeof(msg) + 2;
	char r_msg[size];
	send(sock_cl, msg, size, 0);
	recv(sock_cl, r_msg, size, 0);
	if (strcmp(r_msg, msg))
		throw std::exception("Cannot establish connection");
	Sleep(50);
	return true;
}
int Client::c_send(char * buf, int buf_length) {
	int result = send(sock_cl, buf, buf_length, 0);
	int k = WSAGetLastError();
	if (result == SOCKET_ERROR)
		throw std::exception("Socket error");
	return result;
}
int Client::receive(char * buf, int buf_length) {
	int bytesReceived = recv(sock_cl, buf, buf_length, 0);
	int k = WSAGetLastError();
	if (bytesReceived == SOCKET_ERROR)
		throw std::exception("Socket error");
	return bytesReceived;
}


void Client::connection(int protocol)
{
	hint.sin_family = protocol;
	hint.sin_port = htons(port);
	inet_pton(protocol, ipaddress.c_str(), &hint.sin_addr);

	int connResult = connect(sock_cl, (sockaddr*)&hint, sizeof(hint));

	if (connResult == SOCKET_ERROR)
		throw std::exception("Connection failed");
}
Client::~Client()
{
	closesocket(sock_cl);
	WSACleanup();
}