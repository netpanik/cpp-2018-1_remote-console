#pragma once
#include <Windows.h>
#include <exception>
struct HandleGuard
{
	HANDLE m_handle;

	HandleGuard(HANDLE * hndl):m_handle(hndl) {};
	~HandleGuard() { 
		if (m_handle)
			CloseHandle(m_handle);
	};
};
struct SocketGuard
{
	SOCKET sock;

	SocketGuard(SOCKET  sock_c) :sock(sock_c) {};
	~SocketGuard() {
		if (sock)
			closesocket(sock);
	}
};





class Job
{
	HANDLE hJob;
public:
	Job(LPSECURITY_ATTRIBUTES sa, LPCSTR lpName) { 
		hJob = CreateJobObject(sa, lpName); 
		if (!hJob)
			throw std::exception("Cannot create job object");

		JOBOBJECT_BASIC_LIMIT_INFORMATION info = { 0,0, JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE };
		if (!SetInformationJobObject(hJob,
			JobObjectBasicLimitInformation, &info,
			sizeof(JOBOBJECT_BASIC_LIMIT_INFORMATION)))
			throw std::exception("Cannot set information for object");
	}
	void AssignProcess(HANDLE process) { 
		
		bool tr = AssignProcessToJobObject(hJob, process); 
		if (!tr)
			throw std::exception("Cannot assign process to job");
	}
	~Job() { TerminateJobObject(hJob,0); }
};


class Process
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	HANDLE hDesktop;
	char* desktop_name = "cmd_desk";
public:
	Process() {

		hDesktop = CreateDesktop(desktop_name, NULL, NULL, 0, GENERIC_ALL, NULL);
		if(!hDesktop)
			throw std::exception("Cannot create v.desktop");

		si.lpDesktop = desktop_name;

		if (!CreateProcess(NULL, "cmd", NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
			throw std::exception("Cannot create process");
	}
	HANDLE GetHandle() { return pi.hProcess; }
	void Attach() { if(!AttachConsole(pi.dwProcessId))
		throw std::exception("Cannot attach console");
	}
	bool CheckProcessState() { DWORD res = WaitForSingleObject(pi.hProcess, 0); return res == WAIT_TIMEOUT; }

	~Process() { CloseHandle(hDesktop); }
};

class Console
{
	CONSOLE_SCREEN_BUFFER_INFO BufferInfo;
	SHORT col, row;

	DWORD nLength,lpNumberOfEventsWritten,lpNumberOfEventsRead;
	COORD dwBufferSize, dwBufferCoord;
	SMALL_RECT lpReadRegion;

public:
	Console() {
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

		GetConsoleScreenBufferInfo(hOut, &BufferInfo);
		col = BufferInfo.dwSize.X;
		row = BufferInfo.dwSize.Y;
		dwBufferSize = { col,row};
		dwBufferCoord = { 0,0 };
		lpReadRegion = { 0,0,col,row };
		nLength = 255;
	}
	Console(SHORT cols, SHORT rows, int length) {

		dwBufferSize = { cols,rows };
		dwBufferCoord = { 0,0 };
		lpReadRegion = { 0,0,cols,rows };
		nLength = length;
	}
		void ReadChar(INPUT_RECORD * inputArray, DWORD toRead, DWORD &wasRead) {
			DWORD fdwMode, fdwSaveOldMode;
			HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
			if (hStdin == INVALID_HANDLE_VALUE)
				throw std::exception("Invalid handle");

			if (!GetConsoleMode(hStdin, &fdwSaveOldMode))
				throw std::exception("Invalid handle");

			fdwMode = ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT;
			if (!SetConsoleMode(hStdin, fdwMode))
				std::cout << "Cannot set up console mode";
			ReadConsoleInput(hStdin, inputArray, toRead, &wasRead);
			SetConsoleMode(hStdin, fdwSaveOldMode);
		};
		void SetReadingInfo(SHORT cols, SHORT rows) {
			dwBufferSize = { cols,rows };
			dwBufferCoord = { 0,0 };
			lpReadRegion = { 0,0,cols,rows };
		};
	void ReadInput(INPUT_RECORD *inputArray, DWORD lngth) {
		HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);

		if (!ReadConsoleInput(hIn, inputArray, lngth, &lpNumberOfEventsRead))
			throw std::exception("Cannot read console input buffer");
	}
	void WriteInput(INPUT_RECORD * inputArray, DWORD nLength_) {
		HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);
		if (!WriteConsoleInput(hIn, inputArray, nLength_, &lpNumberOfEventsWritten))
			throw std::exception("Cannot write to console input buffer");
	}
	DWORD ReadOutput(CHAR_INFO * readBuf) {
		
		if (0 == readBuf)
			return dwBufferSize.X * dwBufferSize.Y;

		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

		if(!ReadConsoleOutput(hOut,readBuf,dwBufferSize,dwBufferCoord,&lpReadRegion))
			throw std::exception("Cannot read console output buffer");

	}
	void WriteOutput(CHAR_INFO *lpBuffer) {
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	    CONSOLE_SCREEN_BUFFER_INFO bi = { 0 };
		GetConsoleInfo(bi);
		
		if (dwBufferSize.X > bi.dwSize.X || dwBufferSize.Y > bi.dwSize.Y)
			return;

		if (!WriteConsoleOutput(hOut, lpBuffer, dwBufferSize, dwBufferCoord, &lpReadRegion)){
			throw std::exception("Cannot write to console output buffer");
		}
	}
	void SetConsoleSize(COORD dwSize) {
		DWORD temp;
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		if (!SetConsoleScreenBufferSize(hOut, dwSize)) {
			int k = GetLastError();
			throw std::exception("Cannot set console size");

		}
	}
	void SetCursorPos(COORD cursor) {
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleCursorPosition(hOut, cursor);
	}
	void GetConsoleInfo(CONSOLE_SCREEN_BUFFER_INFO &bi) {
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &bi);
	}
	void GetNumOfInputEvents(DWORD &num) {
		GetNumberOfConsoleInputEvents(GetStdHandle(STD_INPUT_HANDLE), &num);

	}
	void CtrlHandler(BOOL add) {
		if (!SetConsoleCtrlHandler(NULL, add))
			throw std::exception("cannot set up ctrl handler");
	}
	void SetWindow(COORD top, COORD bot) {
		SMALL_RECT window = { top.X,top.Y,bot.Y,bot.X};
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO bi;
		GetConsoleInfo(bi);
		if (window.Right == bi.srWindow.Right && window.Bottom == bi.srWindow.Bottom)
			return;
		if (window.Right > bi.dwMaximumWindowSize.X && window.Right > bi.dwMaximumWindowSize.Y)
			return;
		if (!SetConsoleWindowInfo(hOut, TRUE, &window)) {
			int k = GetLastError();
			throw std::exception("Cannot setup console window info");
		}
	}
};