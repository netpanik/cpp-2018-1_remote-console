#include "crypto.h"
int check_prime(unsigned long int n)
{

	unsigned long int i;

	for (i = 2; i <= sqrt(n); i++)
	{
		if (n%i == 0) {
			return 0;
		}
	}

	return 1;
};
unsigned long int jpl(unsigned long int b, unsigned long int e, unsigned long int m)
{

	unsigned long int c, e1;

	c = 1;
	e1 = 0;

	while (e1<e) {
		e1++;
		c = (b*c) % m;
	}

	return c;
};
unsigned long int o_encrypt(unsigned long int n, unsigned long int e, unsigned long int m)
{
	unsigned long int c, i;

	c = jpl(m, e, n);
	return c;
};
unsigned long int o_decrypt(unsigned long int n, unsigned long int d, unsigned long int c)
{
	unsigned long int m;

	m = jpl(c, d, n);
	return m;
};


void cryptSerialize(char* noCryptStr, char* CryptStr, int lenth, unsigned long int e, unsigned long int n) {
	//for (int i = 0; i < lenth; i++) {
	//	Arr[i] = encrypt(n, e, noCryptStr[i]);
	//}
	//for (int i = 0; i < lenth; i++) {
	//	memcpy(&CryptStr[i * sizeof(unsigned long int)], &Arr[i], sizeof(unsigned long int));
	//}
	unsigned long int tmp;
	for (int i = 0; i < lenth; i++) {
		tmp = o_encrypt(n, e, (unsigned char)noCryptStr[i]);

		memcpy(&CryptStr[i * sizeof(unsigned long int)], &tmp, sizeof(unsigned long int));
	}
};
void cryptDeserialize(char* noCryptStr, char* CryptStr, int lenth, unsigned long int d, unsigned long int n) {
	//for (int i = 0; i < lenth; i++) {
	//	memcpy(&(Arr[i]), &(CryptStr[i * sizeof(unsigned long int)]), sizeof(unsigned long int));
	//}
	//for (int i = 0; i < lenth; i++) {
	//	noCryptStr[i] = decrypt(n, d, Arr[i]);
	//}
	unsigned long int tmp;
	for (int i = 0; i < lenth; i++) {
		memcpy(&(tmp), &(CryptStr[i * sizeof(unsigned long int)]), sizeof(unsigned long int));

		noCryptStr[i] = (char)o_decrypt(n, d, tmp);
	}
};
void generateKey(unsigned long int &n, unsigned long int &e, unsigned long int &d)
{
	double r;
	int a;
	unsigned long int p, q, phin, k;
	// generate 2 prime numbers < 100

	a = 0;

	while (a == 0) {
		p = rand() % 100;
		a = check_prime(p);
	}

	a = 0;

	while (a == 0) {
		q = rand() % 100;
		a = check_prime(q);
	}


	std::cout << "p,q = " << p << " " << q << "\n";

	n = p*q;
	phin = (p - 1)*(q - 1);


	// generate e such that 1<e<phin, e and phin are coprime


	a = 0;

	while (a == 0) {
		r = ((double)rand() / (RAND_MAX));
		e = int(0.1*r*double(phin));
		a = check_prime(e);
	}

	std::cout << "public key, n, e: " << n << " " << e << " \n";


	// generate private key, d

	k = 5;
	while ((1 + k*phin) % e != 0) {
		k++;
	}

	d = (1 + k*phin) / e;

	std::cout << " k, d " << k << " " << d << " \n";
};


