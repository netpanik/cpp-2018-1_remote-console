#pragma once
#include<iostream>
#include<string>
#include "Network.h"
#include "Console.h"


CONSOLE_SCREEN_BUFFER_INFO bi;

void ReadConfig(std::string &login,std::string &password, std::string &port,std::string &IP)
{
	std::ifstream fin("app.config");

	if (fin.is_open()) {
		std::getline(fin, login);
		std::getline(fin, password);
		std::getline(fin, port);
		std::getline(fin, IP);
		fin.close();
	}

}

int main() {

	std::string login, password, port, IP;


	ReadConfig(login, password, port, IP);

	std::cout << "Waiting for connection ... " << std::endl;
	try
	{
		Server server(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		server.binding(AF_INET, atoi(port.c_str()), INADDR_ANY);

		
		server.connection();

		Console console(col,row,length);

		console.SetConsoleSize({ col,row });
		FreeConsole();

		Job job(NULL, NULL);
		Process process;


		process.Attach();



		console.SetConsoleSize({ col,row });


		job.AssignProcess(process.GetHandle());


		Package<CHAR_INFO> ci_pack;
		Package<COORD> cursor_pos;
		Package<std::string> logpass;
		std::vector<COORD> coords(1);
		COORD bufSize = { col,row };

		server.establish("Hello");
		server.receive(logpass);
		if (std::string(logpass.command) == "logpass")
		{
			if(logpass.data[0] == login && logpass.data[1] == password)
			{
				logpass.cleanUp();
				logpass.SetCommand(std::string("Ok"));
				server.c_send(logpass);
			}
			else
			{
				logpass.cleanUp();
				logpass.SetCommand(std::string("Wrong"));
				server.c_send(logpass);
				throw std::exception("Bad logpass");
			}
			}
		
		while (true)
		{


			//Check Process status
			if (!process.CheckProcessState())
				throw std::exception("Process down");



			//Receive internal input buf size
			cursor_pos.cleanUp();
			server.receive<COORD>(cursor_pos);
			if (std::string(cursor_pos.command) == "bufsize")
			{
				console.SetConsoleSize(cursor_pos.data[0]);
			}
		


			
		
			

			//Send output buffer
			std::vector<CHAR_INFO> bufferOutput(console.ReadOutput(0));
			ci_pack.SetCommand(std::string("OutBuffer"));

			if (bufferOutput.size() != 0)
			{
				console.ReadOutput(&bufferOutput[0]);
			}



			ci_pack.SetDataBuffer(bufferOutput);
			int res = server.c_send<CHAR_INFO>(ci_pack);
			Sleep(100);
			res = server.receive<CHAR_INFO>(ci_pack);

			if (std::string(ci_pack.command) != "ok")
				break;
			
			//Send cursor position
			cursor_pos.cleanUp();
			console.GetConsoleInfo(bi);
			console.SetReadingInfo(bi.dwSize.X, bi.dwSize.Y);

			coords[0] = bi.dwCursorPosition;
			cursor_pos.SetDataBuffer(coords);
			cursor_pos.SetCommand(std::string("cursor_pos"));
			server.c_send<COORD>(cursor_pos);


			//Write Input
			Package<INPUT_RECORD> inputRecordsPack;
			int bytesReceived = server.receive<INPUT_RECORD>(inputRecordsPack);

			if (std::string(inputRecordsPack.command) == "InputRecords" && inputRecordsPack.data.size() != 0 )
			{
				console.WriteInput(&inputRecordsPack.data[0], inputRecordsPack.data.size());
			}

			Sleep(100);

		}
	}
	catch (const std::exception &ex)
	{
		std::cout<< ex.what();
		return 1;
	}
	return 0;
}

