#pragma once
#include <WS2tcpip.h>
#include <string>
#include <vector>
#include <crypto.h>
#include "Console.h"
#pragma comment(lib,"ws2_32.lib")

const SHORT col = 120, row = 1000, length = 255;
//guards needed
template <typename T>
struct Package
{
	char command[20];
	unsigned int tailSize;
	std::vector<T> data;

	Package()
	{
		memset(&command, 0, sizeof(command));
		tailSize = 0;
		data.clear();
	}

	void SetCommand(std::string &command_)
	{
		if(command_.size() > sizeof(command))
			throw std::runtime_error("Bad command size");

		memcpy(&command, command_.c_str(), command_.size());
	}

	size_t GetPackSize()
	{
		return sizeof(command) + sizeof(tailSize) + data.size() * sizeof(T);
	}

	size_t GetHeaderSize()
	{
		return sizeof(command) + sizeof(tailSize);
	}

	void serialize(std::vector<char> &rawData)
	{
		tailSize = data.size() * sizeof(T);
		rawData.reserve(this->GetPackSize());
		rawData.insert(rawData.end(), reinterpret_cast<char*>(&command[0]), reinterpret_cast<char*>(&command[0]) + sizeof(command));
		rawData.insert(rawData.end(), reinterpret_cast<char*>(&tailSize), reinterpret_cast<char*>(&tailSize) + sizeof(tailSize));

		if (tailSize)
		{
			char* begin = (char*)&data[0];
			char* end = ((char*)&data[data.size() - 1]) + sizeof(T);
			rawData.insert(rawData.end(), begin, end);
		}
	}

	void deserializeHeader(const std::vector<char> &rawData)
	{
		memcpy(&command[0], &rawData[0], GetHeaderSize());

		if (0 == tailSize)
		{
			data.clear();
			return;
		}
	}

	void SetDataRawBuffer(const std::vector<char> &rawData)
	{
		if (tailSize > rawData.size())
		{
			throw std::runtime_error("Bad package");
		}

		data.assign((T*)&rawData[0], (T*)(&rawData[rawData.size() - 1] + sizeof(char)));
	}

	void SetDataBuffer(const std::vector<T> &buffer)
	{
		data.assign(buffer.begin(), buffer.end());
	}

	void cleanUp()
	{
		memset(&command, 0, sizeof(command));
		tailSize = 0;
		data.clear();
	}
};

class Server
{
	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2);
	sockaddr_in hint, client;
	SOCKET listening, clientSocket;
	unsigned long int n, e, d;
public:
	Server(int protocol, int sock, int flags);
	void key_exchange();
	void cr_serialize(char* str, char * secureStr, char  cNumRead);
	void cr_deserialize(char *secureBuf, char*buf);
	void binding(int family, int port, ULONG addr);
	bool establish(char* msg);
	int c_send(char * buf,int buf_length);
	int receive(char * buf, int buf_length);

	
	
	
	template<typename T>
	int c_send(Package<T> &pack)
	{
		std::vector<char> rawData;
		pack.serialize(rawData);
		int result = send(clientSocket, &rawData[0], rawData.size(), 0);
		if (result == SOCKET_ERROR || result == 0)
			throw std::exception("Socket error");
		return result;
	}

	template<typename T>
	int receive(Package<T> &pack)
	{
		int HeaderSize = pack.GetHeaderSize();
		std::vector<char> rawBuffer;
		rawBuffer.resize(HeaderSize);

		int bytesReceived = recv(clientSocket, &rawBuffer[0], HeaderSize, 0);
		if (bytesReceived == SOCKET_ERROR || bytesReceived == 0)
			throw std::exception("Socket error");

		pack.deserializeHeader(rawBuffer);

		if (0 == pack.tailSize)
			return bytesReceived;

		rawBuffer.resize(pack.tailSize);

		bytesReceived += recv(clientSocket, &rawBuffer[0], pack.tailSize, 0);
		if (bytesReceived == SOCKET_ERROR || bytesReceived == 0)
			throw std::exception("Socket error");

		pack.SetDataRawBuffer(rawBuffer);

		return bytesReceived;
	}

	void connection();
	~Server();
};

class Client
{
	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2);
	std::string ipaddress;
	int port = 54000;
	sockaddr_in hint;
	SOCKET sock_cl;
	unsigned long int n, e, d;
public:
	Client(int protocol, int sock, int flags);
	void addr_set(std::string ip, int x) { ipaddress = ip; port = x;};
	void key_exchange();
	void cr_serialize(char* str, char * secureStr, char  cNumRead);
	void cr_deserialize(char *secureBuf, char*buf);
	bool establish(char* msg);
	int c_send(char * buf, int buf_length);
	int receive(char * buf, int buf_length);
	
	template<typename T>
	int c_send(Package<T> &pack)
	{
		std::vector<char> rawData;
		pack.serialize(rawData);
		int result = send(sock_cl, &rawData[0], rawData.size(), 0);
		if (result == SOCKET_ERROR || result == 0)
			throw std::exception("Socket error");
		return result;
	}
	
	template<typename T>
	int receive(Package<T> &pack)
	{
		int HeaderSize = pack.GetHeaderSize();
		std::vector<char> rawBuffer;
		rawBuffer.resize(HeaderSize);

		int bytesReceived = recv(sock_cl, &rawBuffer[0], HeaderSize, 0);
		if (bytesReceived == SOCKET_ERROR || bytesReceived == 0)
			throw std::exception("Socket error");

		pack.deserializeHeader(rawBuffer);

		rawBuffer.resize(pack.tailSize);
		if (pack.tailSize != 0) {
			bytesReceived += recv(sock_cl, &rawBuffer[0], pack.tailSize, 0);
			if (bytesReceived == SOCKET_ERROR || bytesReceived == 0)
				throw std::exception("Socket error");
			pack.SetDataRawBuffer(rawBuffer);

		}

		return bytesReceived;
	}


	void connection(int protocol);
	~Client();
};







//posible improvement
class ConsoleProcessor
{
	Console console;

public:
	ConsoleProcessor() {};
	ConsoleProcessor(SHORT col, SHORT row) {
		console.SetReadingInfo(col, row);
	}
	void ConsoleSetup(COORD setbufsize, BOOL CtrlHandler) {
		console.SetConsoleSize(setbufsize);
		console.CtrlHandler(FALSE);
	}
	void WriteOutput(Package<CHAR_INFO> pack) {
		console.WriteOutput(&pack.data[0]);
	};
	void ReadOutput(Package<CHAR_INFO> &pack) {
		console.ReadOutput(&pack.data[0]);
	};
	void WriteInput(Package<INPUT_RECORD> pack,DWORD length) {
		console.WriteInput(&pack.data[0],length);
	};
	void ReadInput(Package<INPUT_RECORD> &pack,DWORD length) {
		console.ReadInput(&pack.data[0], length);
	};
	void SetBufSize(Package<COORD> pack) {
		console.SetConsoleSize(pack.data[0]);
	}
	void SetCursorPosition(Package<COORD> pack) {
		console.SetCursorPos(pack.data[0]);
	}
	void SetWindowSize(Package<COORD> pack) {
		console.SetWindow(pack.data[1], pack.data[2]);
	}
	void ReadCharacter(Package<INPUT_RECORD> pack, DWORD numOfEvents, DWORD wasRead) {
		console.GetNumOfInputEvents(numOfEvents);

		if (numOfEvents > 0)
		{
			pack.data.resize(numOfEvents);
			DWORD wasReaded = 0;
			console.ReadChar(&pack.data[0], numOfEvents, wasRead);
		}

	}
	void BufSizeWindowChecker(std::vector<COORD> coords)
	{
		CONSOLE_SCREEN_BUFFER_INFO bi;
		console.GetConsoleInfo(bi);
		coords[0] = bi.dwSize;
		coords[1] = { bi.srWindow.Top,bi.srWindow.Left };
		coords[2] = { bi.srWindow.Bottom,bi.srWindow.Right };
		console.SetReadingInfo(bi.dwSize.X, bi.dwSize.Y);

	}

};
