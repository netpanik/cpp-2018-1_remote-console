#pragma once
#include<iostream>
#include<math.h>
#include<cstdlib>
#include<fstream>

int check_prime(unsigned long int n);

unsigned long int jpl(unsigned long int b, unsigned long int e, unsigned long int m);

unsigned long int o_encrypt(unsigned long int n, unsigned long int e, unsigned long int m);

unsigned long int o_decrypt(unsigned long int n, unsigned long int d, unsigned long int c);



void cryptSerialize(char* noCryptStr, char* CryptStr, int lenth, unsigned long int e, unsigned long int n);
void cryptDeserialize(char* noCryptStr, char* CryptStr, int lenth, unsigned long int d, unsigned long int n);
void generateKey(unsigned long int &n, unsigned long int &e, unsigned long int &d);