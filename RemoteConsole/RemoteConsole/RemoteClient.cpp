#pragma once
#include<iostream>
#include "Network.h"
#include "Console.h"




CONSOLE_SCREEN_BUFFER_INFO bi;
void ReadConfig(std::string &login, std::string &password, std::string &port, std::string &IP)
{
	std::ifstream fin("app.config");

	if (fin.is_open()) {
		std::getline(fin, login);
		std::getline(fin, password);
		std::getline(fin, port);
		std::getline(fin, IP);
		fin.close();
	}

}
int main() {
	std::string login, password, port, IP;
	ReadConfig(login, password, port, IP);


	Console console(col,row,length);

	Client client(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	client.addr_set(IP,atoi(port.c_str()));

	console.SetConsoleSize({col,row});

	try {
		client.connection(AF_INET);
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}

	Package<INPUT_RECORD> ir_pack;

	Package<CHAR_INFO> ci_pack;

	Package<COORD> cursor_pos;
	Package<std::string> logpass;
	COORD bufSize = { col,row };
	std::vector<COORD> coords(1);
	try {
		client.establish("Hello");
		
		//client.key_exchange();
		logpass.SetCommand(std::string("logpass"));
		logpass.data.resize(2);
		logpass.data[0] = login;
		logpass.data[1] = password;
		client.c_send(logpass);
		logpass.cleanUp();
		client.receive(logpass);
		if (std::string(logpass.command) != "Ok")
			throw std::exception("Invalid login or password");
		do
		{
			//Get console buf size and send it


			cursor_pos.cleanUp();
			cursor_pos.SetDataBuffer(coords);
			cursor_pos.SetCommand(std::string("bufsize"));
			console.GetConsoleInfo(bi);
			coords[0] = bi.dwSize;
			console.SetReadingInfo(bi.dwSize.X, bi.dwSize.Y);
			cursor_pos.cleanUp();
			cursor_pos.SetDataBuffer(coords);
			cursor_pos.SetCommand(std::string("bufsize"));
			client.c_send<COORD>(cursor_pos);


			//receive output buffer	
				Package<CHAR_INFO> pack;
				Sleep(100);

				int bytesReceived = client.receive<CHAR_INFO>(pack);

				if (std::string(pack.command) == "OutBuffer")
				{
					console.WriteOutput(&pack.data[0]);
				} 
				pack.cleanUp();
				pack.SetCommand(std::string("ok"));
				bytesReceived = client.c_send<CHAR_INFO>(pack);


				//Receive cursor position
				cursor_pos.cleanUp();
				bytesReceived = client.receive<COORD>(cursor_pos);
				if (std::string(cursor_pos.command) == "cursor_pos")
					console.SetCursorPos(cursor_pos.data[0]);


				//Get input records

				//TODO ReadCharacter
				Package<INPUT_RECORD> inputRecordsPack;

				DWORD numOfEvents = 0;
				console.GetNumOfInputEvents(numOfEvents);
				if (numOfEvents > 0)
				{
					inputRecordsPack.data.resize(numOfEvents);
					DWORD wasReaded = 0;
					console.ReadChar(&inputRecordsPack.data[0], numOfEvents, wasReaded);
				}


				inputRecordsPack.SetCommand(std::string("InputRecords"));
				client.c_send<INPUT_RECORD>(inputRecordsPack);
			
			
				Sleep(100);

		} while (true);
	}
	catch (const std::exception &ex)
	{
		MessageBox(NULL, ex.what(), NULL, MB_OK);
		return 1;
	}
	////close
	return 0;
}
