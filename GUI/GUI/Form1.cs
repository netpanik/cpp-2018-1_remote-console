﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;


namespace GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private bool IPvalidate(string IP)
        {
            //string pattern = "[0-2]?\\d?\\d?.[0-2]?\\d?\\d?.[0-2]?\\d?\\d?.[0-2]?\\d?\\d?";
            // Regex rg= new Regex("[0-2]?\\d?\\d?.[0-2]?\\d?\\d?.[0-2]?\\d?\\d?.[0-2]?\\d?\\d?");
            Regex rg = new Regex("[0-2]\\d?\\d?");
            return rg.IsMatch(IP); 
        }
        private bool PortValidate(string port)
        {
            if(port!="")
            return (Int64.Parse(port.ToString())>1024 && Int64.Parse(port.ToString())<64000);
            return false;
        }

        private void btnStart_Click(object sender, EventArgs e)
        { 
            bool ready = true;
            if (!IPvalidate(IP1.Text.ToString())|| !IPvalidate(IP2.Text.ToString()) || !IPvalidate(IP3.Text.ToString()) || !IPvalidate(IP4.Text.ToString()))
            {
                MessageBox.Show("IP is not valid!");
                ready = false;
            }
            if (!PortValidate(maskedTextBoxPort.Text))
            {
                MessageBox.Show("Port is not valid!");
                ready = false;
            }
            if (textLogin.Text == "" || textPassword.Text == "")
            {
                MessageBox.Show("Input login and password!");
                ready = false;
            }
           
            if (rbClient.Enabled == true&& ready ==true)
            {
                //TODO: exec client
                FileStream file1 = new FileStream("app.config", FileMode.Create); //создаем файловый поток
                StreamWriter writer = new StreamWriter(file1); //создаем «потоковый писатель» и связываем его с файловым потоком 
                                                               // writer.Write("текст"); //записываем в файл
                writer.WriteLine(textLogin.Text.ToString());//login
                writer.WriteLine(textPassword.Text.ToString());//password
                writer.WriteLine(maskedTextBoxPort.Text.ToString());//port
                writer.WriteLine(IP1.Text.ToString() + '.' + IP2.Text.ToString() + '.' + IP3.Text.ToString() + '.' + IP4.Text.ToString());//IP
                writer.Close(); //закрываем поток. Не закрыв поток, в файл ничего не запишется 
                Process.Start("Client.exe");
                Application.Exit();
            }
            if (rbServer.Enabled == true && ready == true)
            {
                //TODO: exec server
                FileStream file1 = new FileStream("app.config", FileMode.Create); //создаем файловый поток
                StreamWriter writer = new StreamWriter(file1); //создаем «потоковый писатель» и связываем его с файловым потоком 
                                                               // writer.Write("текст"); //записываем в файл
                writer.WriteLine(textLogin.Text.ToString());//login
                writer.WriteLine(textPassword.Text.ToString());//password
                writer.WriteLine(maskedTextBoxPort.Text.ToString());//port
                writer.WriteLine(IP1.Text.ToString()+'.' + IP2.Text.ToString() + '.' + IP3.Text.ToString() + '.' + IP4.Text.ToString());//IP
                writer.Close(); //закрываем поток. Не закрыв поток, в файл ничего не запишется 
                Process.Start("Server.exe");
                Application.Exit();
            }
           
            

        }

        private void textPort_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textPort_KeyDown(object sender, KeyEventArgs e)
        {
    
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox3_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
