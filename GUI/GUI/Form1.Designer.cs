﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbServer = new System.Windows.Forms.RadioButton();
            this.rbClient = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.textLogin = new System.Windows.Forms.TextBox();
            this.labelIP = new System.Windows.Forms.Label();
            this.PortLabel = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.maskedTextBoxPort = new System.Windows.Forms.MaskedTextBox();
            this.IP1 = new System.Windows.Forms.MaskedTextBox();
            this.IP2 = new System.Windows.Forms.MaskedTextBox();
            this.IP3 = new System.Windows.Forms.MaskedTextBox();
            this.IP4 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbClient);
            this.groupBox1.Controls.Add(this.rbServer);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(70, 71);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // rbServer
            // 
            this.rbServer.AutoSize = true;
            this.rbServer.Location = new System.Drawing.Point(6, 19);
            this.rbServer.Name = "rbServer";
            this.rbServer.Size = new System.Drawing.Size(56, 17);
            this.rbServer.TabIndex = 0;
            this.rbServer.TabStop = true;
            this.rbServer.Text = "Server";
            this.rbServer.UseVisualStyleBackColor = true;
            // 
            // rbClient
            // 
            this.rbClient.AutoSize = true;
            this.rbClient.Location = new System.Drawing.Point(6, 42);
            this.rbClient.Name = "rbClient";
            this.rbClient.Size = new System.Drawing.Size(51, 17);
            this.rbClient.TabIndex = 1;
            this.rbClient.TabStop = true;
            this.rbClient.Text = "Client";
            this.rbClient.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Login";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(83, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password";
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(136, 53);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(93, 20);
            this.textPassword.TabIndex = 3;
            // 
            // textLogin
            // 
            this.textLogin.Location = new System.Drawing.Point(136, 28);
            this.textLogin.Name = "textLogin";
            this.textLogin.Size = new System.Drawing.Size(93, 20);
            this.textLogin.TabIndex = 4;
            // 
            // labelIP
            // 
            this.labelIP.AutoSize = true;
            this.labelIP.Location = new System.Drawing.Point(88, 86);
            this.labelIP.Name = "labelIP";
            this.labelIP.Size = new System.Drawing.Size(17, 13);
            this.labelIP.TabIndex = 5;
            this.labelIP.Text = "IP";
            // 
            // PortLabel
            // 
            this.PortLabel.AutoSize = true;
            this.PortLabel.Location = new System.Drawing.Point(15, 86);
            this.PortLabel.Name = "PortLabel";
            this.PortLabel.Size = new System.Drawing.Size(26, 13);
            this.PortLabel.TabIndex = 6;
            this.PortLabel.Text = "Port";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(11, 112);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(218, 43);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // maskedTextBoxPort
            // 
            this.maskedTextBoxPort.Location = new System.Drawing.Point(47, 83);
            this.maskedTextBoxPort.Mask = "00000";
            this.maskedTextBoxPort.Name = "maskedTextBoxPort";
            this.maskedTextBoxPort.Size = new System.Drawing.Size(35, 20);
            this.maskedTextBoxPort.TabIndex = 10;
            this.maskedTextBoxPort.ValidatingType = typeof(int);
            // 
            // IP1
            // 
            this.IP1.Location = new System.Drawing.Point(111, 83);
            this.IP1.Mask = "000";
            this.IP1.Name = "IP1";
            this.IP1.Size = new System.Drawing.Size(25, 20);
            this.IP1.TabIndex = 11;
            this.IP1.ValidatingType = typeof(int);
            this.IP1.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            // 
            // IP2
            // 
            this.IP2.Location = new System.Drawing.Point(142, 83);
            this.IP2.Mask = "000";
            this.IP2.Name = "IP2";
            this.IP2.Size = new System.Drawing.Size(25, 20);
            this.IP2.TabIndex = 12;
            this.IP2.ValidatingType = typeof(int);
            // 
            // IP3
            // 
            this.IP3.Location = new System.Drawing.Point(173, 83);
            this.IP3.Mask = "000";
            this.IP3.Name = "IP3";
            this.IP3.Size = new System.Drawing.Size(25, 20);
            this.IP3.TabIndex = 13;
            this.IP3.ValidatingType = typeof(int);
            this.IP3.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox3_MaskInputRejected);
            // 
            // IP4
            // 
            this.IP4.Location = new System.Drawing.Point(204, 83);
            this.IP4.Mask = "000";
            this.IP4.Name = "IP4";
            this.IP4.Size = new System.Drawing.Size(25, 20);
            this.IP4.TabIndex = 14;
            this.IP4.ValidatingType = typeof(int);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 184);
            this.Controls.Add(this.IP4);
            this.Controls.Add(this.IP3);
            this.Controls.Add(this.IP2);
            this.Controls.Add(this.IP1);
            this.Controls.Add(this.maskedTextBoxPort);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.PortLabel);
            this.Controls.Add(this.labelIP);
            this.Controls.Add(this.textLogin);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbClient;
        private System.Windows.Forms.RadioButton rbServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.TextBox textLogin;
        private System.Windows.Forms.Label labelIP;
        private System.Windows.Forms.Label PortLabel;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPort;
        private System.Windows.Forms.MaskedTextBox IP1;
        private System.Windows.Forms.MaskedTextBox IP2;
        private System.Windows.Forms.MaskedTextBox IP3;
        private System.Windows.Forms.MaskedTextBox IP4;
    }
}

